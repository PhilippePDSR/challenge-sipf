# Run manually
```cmd
mongod --dbpath D:\MongoDB\
```

# Run with docker
```cmd
docker run
	--name mongodb
	-p 27017:27017
	-v C:/mongodb/db:/data/db/
    mongo
```