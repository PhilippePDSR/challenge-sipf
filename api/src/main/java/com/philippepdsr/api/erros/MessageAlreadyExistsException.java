//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api.erros;

//##################################################
//###                DEPENDANCES                 ###
//##################################################

//Internals
import com.philippepdsr.api.models.Message;

//##################################################
//###     CLASS MESSAGES-NOT-FOUND-EXCEPTION     ###
//##################################################

/**
 * Thrown when message already exist.
 * 
 * @author Philippe Pérez de San Roman
 */
public class MessageAlreadyExistsException extends Exception
{

	// ****************************************
	// ***              FIELDS              ***
	// ****************************************
	
	/**
	 * Something required for serialization.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Unique identified of the message that was not found ({@code String}).
	 */
	public String id;
	
	/**
	 * {@code Message} that could not be found ({@code Message}).
	 */
	public Message message;

	// ****************************************
	// ***            CONSTRUCTOR           ***
	// ****************************************
	
	/**
	 * Initializes a new {@code MessageAlreadyExistsException} with the given {@code id}.
	 * 
	 * @param id ({@code String}): Unique identified of the message.
	 */
	public MessageAlreadyExistsException ( String id )
	{
		super( "A message with an id equal to ObjectID('" + id + "') already exists" );
		this.id = id;
		this.message = null;
	}

	/**
	 * Initializes a new {@code MessageAlreadyExistsException} with the given {@code message}.
	 * 
	 * @param message ({@code Message}): Message that was not found.
	 */
	public MessageAlreadyExistsException ( Message message )
	{
		super( "A message with an id equal to ObjectID('" + message.getId() + "') already exists" );
		this.id = message.getId();
		this.message = message;
	}

} // class MessageAlreadyExistsException
