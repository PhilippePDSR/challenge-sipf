//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api.erros;

//##################################################
//###                DEPENDANCES                 ###
//##################################################

//Internals
import com.philippepdsr.api.models.Message;

//##################################################
//###     CLASS MESSAGES-NOT-FOUND-EXCEPTION     ###
//##################################################

/**
 * Thrown when message cannot be found in the database.
 * 
 * @author Philippe Pérez de San Roman
 */
public class MessageNotFoundException extends Exception
{

	// ****************************************
	// ***              FIELDS              ***
	// ****************************************
	
	/**
	 * Something required for serialization.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Unique identified of the message that was not found ({@code String}).
	 */
	public String id;
	
	/**
	 * {@code Message} that could not be found ({@code Message}).
	 */
	public Message message;

	// ****************************************
	// ***            CONSTRUCTOR           ***
	// ****************************************
	
	/**
	 * Initializes a new {@code MessageNotFoundException} with the given {@code id}.
	 * 
	 * @param id ({@code String}): Unique identified of the message.
	 */
	public MessageNotFoundException ( String id )
	{
		super( "Their is no message with an id equal to ObjectID('" + id + "')" );
		this.id = id;
		this.message = null;
	}

	/**
	 * Initializes a new {@code MessageNotFoundException} with the given {@code message}.
	 * 
	 * @param message ({@code Message}): Message that was not found.
	 */
	public MessageNotFoundException ( Message message )
	{
		super( "Their is no message with an id equal to ObjectID('" + message.getId() + "')" );
		this.id = message.getId();
		this.message = message;
	}

} // class MessageNotFoundException
