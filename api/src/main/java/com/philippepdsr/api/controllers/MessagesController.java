//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api.controllers;

//##################################################
//###                DEPENDANCES                 ###
//##################################################

// Externals
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// Internals
import com.philippepdsr.api.erros.MessageAlreadyExistsException;
import com.philippepdsr.api.erros.MessageNotFoundException;
import com.philippepdsr.api.models.Message;
import com.philippepdsr.api.models.MessagesRequestParameters;
import com.philippepdsr.api.services.MessagesService;

// ##################################################
// ###         CLASS MESSAGES-CONTROLLER          ###
// ##################################################

/**
 * The {@code MessageController} class defines the endpoints (i.e. roots)
 * used to create, read, update and delete messages using the API.
 * 
 * @author Philippe Pérez de San Roman
 */
@RestController
public class MessagesController
{
	
	// ****************************************
	// ***              FIELDS              ***
	// ****************************************
	
	/**
	 * Service used to create, read, update and delete messages ({@code MessagesService}).
	 */
    @Autowired
    private MessagesService service;

	// ****************************************
	// ***              METHODS             ***
	// ****************************************
    
    /**
     * Creates a new message with the given body.
     * The {@code createdTime}, {@code lastModifiedTime} are modified before creating the message in the database.
     * Thus the message in the response will be different compared to the message received in the request.
     * 
     * @param message ({@code Message}): Body of the request with the message to create in JSON format.
     * @return        ({@code Message}): Response with the created message in JSON format in the body.
     */
	@CrossOrigin()
	@PostMapping( "/message" )
	public Message createMessage ( @RequestBody Message message )
		throws MessageAlreadyExistsException
	{
		return this.service.createMessage( message );
	}
	
	/**
	 * Reads the message with the given {@code id} and returns it in the response body.
	 * 
	 * @param id ({@code String}): Id of the message to read.
	 * @return  ({@code Message}): Response body with the message in JSON format.
	 * @throws MessageNotFoundException: If no message with the given {@code id} exists in the database.
	 */
	@CrossOrigin()
	@GetMapping( "/message/{id}" )
	public Message getMessage ( @PathVariable("id") final String id )
		throws MessageNotFoundException
	{
		return this.service.readMessage( id );
	}

	/**
	 * Reads messages satisfying the given messages request parameters in the database.
	 * 
	 * @param filterByCreatedDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code createdDate}.
	 * @param fromCreatedDate ({@code Date}): Lower bound of the created date span.
	 * @param toCreatedDate ({@code Date}): Upper bound of the created date span.
	 * @param filterByLastModifiedDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code lastModifiedDate}.
	 * @param fromLastModifiedDate ({@code Date}): Lower bound of the last modified date span.
	 * @param toLastModifiedDate ({@code Date}): Upper bound of the last modified date span.
	 * @param filterByExpirationDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code expirationDate}.
	 * @param fromExpirationDate ({@code Date}): Lower bound of the expiration date span.
	 * @param toExpirationDate ({@code Date}): Upper bound of the expiration date span.
	 * @param filterByTags ({@code boolean}): If {@code true} messages to query are filtered using their {@code tags}.
	 * @param tagsList ({@code String}): Comma separated list of tags. Messages containing the given tags are included in the query. Otherwise they are excluded.
	 * 
	 * @return ({@codeIterable<Message>}): Found messages.
	 */
	@CrossOrigin()
	@GetMapping( "/messages" )
	public Iterable<Message> getMessages (
			@RequestParam final boolean filterByCreatedDate,
			@RequestParam final Date fromCreatedDate,
			@RequestParam final Date toCreatedDate,
			@RequestParam final boolean filterByLastModifiedDate,
			@RequestParam final Date fromLastModifiedDate,
			@RequestParam final Date toLastModifiedDate,
			@RequestParam final boolean filterByExpirationDate,
			@RequestParam final Date fromExpirationDate,
			@RequestParam final Date toExpirationDate,
			@RequestParam final boolean filterByTags,
			@RequestParam final String tagsList
			)
	{		
		MessagesRequestParameters messageQuery = new MessagesRequestParameters(
				filterByCreatedDate,
				fromCreatedDate,
				toCreatedDate,
				filterByLastModifiedDate,
				fromLastModifiedDate,
				toLastModifiedDate,
				filterByExpirationDate,
				fromExpirationDate,
				toExpirationDate,
				filterByTags,
				tagsList
				);
		
		return this.service.readAllMessages( messageQuery );
	}
	
	/**
	 * Update a message found in JSON format in the body of the request and returns it in the response as a JSON object.
     * The {@code lastModifiedTime} is modified before updating the message in the database.
     * Thus the message in the response will be different compared to the message received in the request.
	 * 
	 * @param id       ({@code String}): Id of the message to update.
	 * @param message ({@code Message}): Message in JSON format found in the body of the request.
	 * @return        ({@code Message}): Message in JSON format sent in the body of the response.
	 * @throws MessageNotFoundException: If no message with the given {@code id} exists in the database.
	 */
	@CrossOrigin()
	@PutMapping( "/message/{id}" )
	public Message updateMessage ( @PathVariable("id") final String id, @RequestBody Message message )
		throws MessageNotFoundException
	{
		return this.service.updateMessage( id, message );
	}
	
	/**
	 * Deletes a message with the given {@code id} in the database.
	 * 
	 * @param id ({@code String}): Id of the message to delete.
	 * @throws MessageNotFoundException: If no message with the given {@code id} exists in the database.
	 */
	@CrossOrigin()
	@DeleteMapping( "/message/{id}" )
	public void deleteMessage ( @PathVariable("id") final String id )
		throws MessageNotFoundException
	{
		this.service.deleteMessage( id );
	}
	
	/**
	 * Handles {@code NotFoundException}s thrown by methods of this controller.
	 *  
	 * @param exception (@code MessageNotFoundException}): Exception that was thrown.
	 * @return {@code ResponseEntity<String>}: Response describing the error that occurred.
	 */
	@ExceptionHandler( MessageNotFoundException.class )
    public ResponseEntity<String> handleException ( MessageNotFoundException exception )
	{
        return new ResponseEntity<String>( exception.toString(), HttpStatus.NOT_FOUND );
    }

	/**
	 * Handles {@code MessageAlreadyExistsException}s thrown by methods of this controller.
	 *  
	 * @param exception (@code MessageAlreadyExistsException}): Exception that was thrown.
	 * @return {@code ResponseEntity<String>}: Response describing the error that occurred.
	 */
	@ExceptionHandler( MessageAlreadyExistsException.class )
	public ResponseEntity<String> handleException ( MessageAlreadyExistsException exception )
	{
		return new ResponseEntity<String>( exception.toString(), HttpStatus.CONFLICT );
	}
	
} // class MessagesController