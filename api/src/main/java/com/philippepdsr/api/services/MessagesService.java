//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api.services;


//##################################################
//###                DEPENDANCES                 ###
//##################################################

// Externals
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.data.mongodb.core.MongoTemplate;

// Internals
import com.philippepdsr.api.erros.MessageAlreadyExistsException;
import com.philippepdsr.api.erros.MessageNotFoundException;
import com.philippepdsr.api.models.Message;
import com.philippepdsr.api.models.MessagesRequestParameters;
import com.philippepdsr.api.repositories.MessagesRepository;

//##################################################
//###           CLASS MESSAGES-SERVICE           ###
//##################################################

/**
 * Service used to interact with the messages database from the roots.
 * 
 * @author Philippe Pérez de San Roman
 */
@Component()
public class MessagesService 
{
	// ****************************************
	// ***              FIELDS              ***
	// ****************************************
	
	/**
	 * Repository used to store messages ({@code MessagesRepository}).
	 */
	@Autowired
	MessagesRepository repository;

	@Autowired
	MongoTemplate template;
	
	// ****************************************
	// ***              METHODS             ***
	// ****************************************
	
	/**
	 * Creates a message in the database.
     * The {@code createdTime}, {@code lastModifiedTime} are modified before creating the message in the database.
     * Thus the returned message will be different compared to the given message.
     * 
	 * @param message ({@code Message}): Message to create.
	 * @return        ({@code Message}): Message once created.
	 */
	public Message createMessage ( Message message )
		throws MessageAlreadyExistsException
	{
		String id = message.getId();
		
		// If the is is specified check it does not conflict with existing messages
		if ( id != "" )
		{
			// Attemp to find the message
			Optional<Message> result = this.repository.findById( message.getId() );
			
			// If the message exists, raise an error
			if ( result.isPresent() )
			{
				throw new MessageAlreadyExistsException( message );
			}
		}
		else
		{
			message.setId( null );
		}
		
		// Set creation date and last modified date on the message
		Date now = new Date();
		message.setCreatedDate( now );
		message.setLastModifiedDate( now );
		
		// Create the message in the database and return it
		return this.repository.insert( message );
	}
	
	/**
	 * Reads the message with the given {@code id}.
	 * 
	 * @param id ({@code String}): Unique identified of the message.
	 * @return  ({@code Message}): The message if found.
	 * @throws MessageNotFoundException: If no message with the given {@code id} exists in the database.
	 */
	public Message readMessage ( String id )
		throws MessageNotFoundException
	{
		// Attemp to find the message
		Optional<Message> result = this.repository.findById( id );
		
		// Throw an error if the message is not found
		if ( !result.isPresent() )
		{
			throw new MessageNotFoundException( id );
		}
		
		// Return the found message
		return result.get();
	}
	
	/**
	 * Reads all the messages in the database.
	 * 
	 * @return ({@code Iterable<Message>}): List of messages, possibly empty.
	 */
	public Iterable<Message> readAllMessages ( MessagesRequestParameters messagesRequestParameters )
	{		
		if ( messagesRequestParameters.areAllFiltersDisabled() )
		{
			return this.repository.findAll();
		}
		else
		{
			return this.template.find(
					messagesRequestParameters.toQuery(),
					Message.class
					);
		}
	}

	/**
	 * Updates a message with the given value using field of the given message.
	 * {@code null} or empty string fields in {@code message} are ignored.
	 * the {@code lastModifiedTime} is also automatically updated.
	 * 
	 * @param id       ({@code String}): Unique identified of the message to update.
	 * @param message ({@code Message}): Message defining the fields to update.
	 * @return        ({@code Message}): Message once updated.
	 * @throws MessageNotFoundException: If no message with the given {@code id} exists in the database.
	 */
	public Message updateMessage ( String id, Message message )
		throws MessageNotFoundException
	{
		// Attempt to read the existing message
		Message existing = this.readMessage( id );
		
		// Update the existing message with the given message fields.
		// null or empty string fields in 'message' are ignored.
		// the lastModifiedTime is also automatically updated.
		existing.update( message );
		
		// Save the message in the database and return it.
		return this.repository.save( existing );
	}
	
	/**
	 * Deletes a message with the given {@code id}.
	 * 
	 * @param id ({@code String}): Unique identified of the message to delete.
	 * @throws MessageNotFoundException: If no message with the given {@code id} exists in the database.
	 */
	public void deleteMessage ( String id )
		throws MessageNotFoundException
	{
		// Attempt to read the message and check if it exists
		Message message = this.readMessage( id );
		
		// Delete the message
		this.repository.delete( message );
	}

} // class MessagesService
