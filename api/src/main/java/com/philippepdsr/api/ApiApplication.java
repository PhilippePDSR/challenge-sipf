//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api;

//##################################################
//###                DEPENDANCES                 ###
//##################################################

// Externals
// Externals
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//##################################################
//###           CLASS API-APPLICATION            ###
//##################################################

/**
 * Main class of the API Application with the entry point of the executable jar file.
 * 
 * @author Philippe Pérez de San Roman
 */
@SpringBootApplication
public class ApiApplication //implements CommandLineRunner  
{	
	
	// ****************************************
	// ***          STATIC-METHODS          ***
	// ****************************************
	
	/**
	 * Entry point of the API Application that runs the Spring Boot application.
	 * This starts the server for the message API.
	 * 
	 * @param args ({@code String[]}): Command line arguments.
	 */
	public static void main ( String[] args )
	{
		SpringApplication.run( ApiApplication.class, args );
	}
	
} // class ApiApplication