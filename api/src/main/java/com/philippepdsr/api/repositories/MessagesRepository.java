//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api.repositories;

//##################################################
//###                DEPENDANCES                 ###
//##################################################

// Externals
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

// Internals
import com.philippepdsr.api.models.Message;

//##################################################
//###         CLASS MESSAGES-REPOSITORY          ###
//##################################################


/**
 * Repository used to create, read, update and delete messages from the MongoDB database.
 * 
 * @author Philippe Pérez de San Roman
 */
@Repository
public interface MessagesRepository extends MongoRepository<Message, String>
{}
