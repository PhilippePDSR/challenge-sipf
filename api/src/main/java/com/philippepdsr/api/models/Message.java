//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api.models;

//##################################################
//###                DEPENDANCES                 ###
//##################################################

// Externals
import java.util.Date;
import lombok.Data;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.*;

//##################################################
//###                CLASS MESSAGE               ###
//##################################################

/**
 * <p>Models {@code Message}s used in this application.</p>
 * <p>
 * It contains the following fields:
 * <ul>
 * 	<li>id ({@code String})</li>
 * 	<li>createdDate ({@code Date})</li>
 * 	<li>lastModifiedDate ({@code Date})</li>
 * 	<li>text ({@code String})</li>
 * 	<li>iconUrl ({@code String})</li>
 * 	<li>link ({@code String})</li>
 * 	<li>tags ({@code String[]})</li>
 * 	<li>expirationDate ({@code Date})</li>
 * </ul>
 * </p>
 * @author Philippe Pérez de San Roman
 */
@Data
@Document( "messages" )
public class Message
{

	// ****************************************
	// ***              FIELDS              ***
	// ****************************************
	
	/**
	 * Unique identifier of the message ({@code String}).
	 */
	@Id
	private String id;
	
	/**
	 * Date and time when the message was first created ({@code Date}).
	 */
	@CreatedDate
	private Date createdDate;
	
	/**
	 * Date and time when the message was most recently modified ({@code Date}).
	 */
	@LastModifiedDate
	private Date lastModifiedDate;
	
	/**
	 * Text displayed in the body of the message ({@code String}).
	 */
	private String text;
	
	/**
	 * <p>URL of the icon displayed in the body of the message ({@code String})</p>.
	 */
	private String iconUrl;
	
	/**
	 * Link to a page associated to this message (@code{String}).
	 */
	private String link;
	
	/**
	 * Array of tags used to filter this message ({@code String[]}).
	 */
	private String[] tags;
	
	/**
	 * Date and time after which the message is no longer displayed nor return when querying the API ({@code Date}).
	 */
	private Date expirationDate;

	// ****************************************
	// ***           CONSTRUCTORS           ***
	// ****************************************
	
	/**
	 * Initializes a new message with all fields initialized with default values.
	 * This constructor is required by Spring to deserialize request bodies.
	 */
	public Message ()
	{
		this.setId( "" );
		this.setCreatedDate( null );
		this.setLastModifiedDate( null );
		this.setText( "" );
		this.setIconUrl( "" );
		this.setLink( "" );
		this.setTags( new String[0] );
		this.setExpirationDate( null );	
	}

	/**
	 * Initializes a new message in full.
	 * 
	 * @param id             ({@code String}): Unique identifier of the message.
	 * @param createdDate      ({@code Date}): Date and time when the message was first created.
	 * @param lastModifiedDate ({@code Date}): Date and time when the message was most recently modified.
	 * @param text           ({@code String}): Text displayed in the body of the message.
	 * @param iconUrl        ({@code String}): URL of the icon displayed in the body of the message.
	 * @param link            (@code{String}): Link to a page associated to this message.
	 * @param tags         ({@code String[]}): Array of tags used to filter this message.
	 * @param expirationDate   ({@code Date}): Date and time after which the message is no longer displayed nor return when querying the API.
	 */
	public Message (
		String id,
		Date createdDate,
		Date lastModifiedDate,
		String text,
		String iconUrl,
		String link,
		String[] tags,
		Date expirationDate
		)
	{
		this.setId( id );
		this.setCreatedDate( createdDate );
		this.setLastModifiedDate( lastModifiedDate );
		this.setText( text );
		this.setIconUrl( iconUrl );
		this.setLink( link );
		this.setTags( tags.clone() );
		this.setExpirationDate( expirationDate );		
	}

	// ****************************************
	// ***             METHODS              ***
	// ****************************************
	
	/**
	 * Updates the fields of this message using those of an {@code other} message.
	 * Fields with {@code null} value or empty strings in {@code other} are ignored.
	 * The {@code lastModifiedDate} is also automatically updated with the current time.
	 * 
	 * @param other ({@code Message}): The message from which to get updated values.
	 * @return ({@code Message}): This message once updated.
	 */
	public Message update ( Message other )
	{
		// Update the text ?
		String text = other.getText();
		if ( text != null && !text.isEmpty() )
		{
			this.setText( other.getText() );
		}
		
		// Update the icon URL ?
		String iconUrl = other.getIconUrl();
		if ( iconUrl != null && !iconUrl.isEmpty() )
		{
			this.setIconUrl( iconUrl );
		}
		
		// Update the icon URL ?
		String link = other.getLink();
		if ( link != null && !link.isEmpty() )
		{
			this.setLink( link );
		}
		
		// Update the tags ?
		String[] tags = other.getTags();
		if ( tags != null && tags.length > 0 )
		{
			this.setTags( tags.clone() );
		}
		
		// Update the expiration date ?
		Date expirationDate = other.getExpirationDate();
		if ( expirationDate != null )
		{
			this.setExpirationDate( expirationDate );
		}
		
		// Update the last modified date
		this.setLastModifiedDate( new Date() );
		
		// Returns this message
		return this;
	}
	
} // class Message
