//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api.models;

import java.util.Arrays;

//##################################################
//###                DEPENDANCES                 ###
//##################################################

// Externals
import java.util.Date;
import lombok.Data;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

//##################################################
//###      CLASS MESSAGES-REQUEST-PARAMETERS     ###
//##################################################

/**
 * Class used to filter a message query and sort the result.
 *  
 * @author Philippe Pérez de San Roman
 */
@Data
public class MessagesRequestParameters
{
	
	// ****************************************
	// ***              FIELDS              ***
	// ****************************************
	
	/**
	 * If {@code true} messages to query are filtered using their {@code createdDate}.
	 * In this case the {@code createdDateSpan} property must also be specified.
	 */
	private boolean filterByCreatedDate;
	
	/**
	 * Messages created within this date span are included in the query.
	 * Otherwise they are excluded.
	 */
	private DateSpan createdDateSpan;
	
	/**
	 * If {@code true} messages to query are filtered using their {@code lastModifiedDate}.
	 * In this case the {@code lastModifiedDateSpan} property must also be specified.
	 */
	private boolean filterByLastModifiedDate;
	
	/**
	 * Messages lastly modified within this date span are included in the query.
	 * Otherwise they are excluded.
	 */
	private DateSpan lastModifiedDateSpan;

	/**
	 * If {@code true} messages to query are filtered using their {@code expirationDate}.
	 * In this case the {@code expirationDateSpan} property must also be specified.
	 */
	private boolean filterByExpirationDate;
	
	/**
	 * Messages expiring within this date span are included in the query.
	 * Otherwise they are excluded.
	 */
	private DateSpan expirationDateSpan;
	
	/**
	 * If {@code true} messages to query are filtered using their {@code tags}.
	 * In this case the {@code tags} property must also be specified.
	 */
	private boolean filterByTags;
	
	/**
	 * Messages containing the given tags are included in the query.
	 * Otherwise they are excluded.
	 */
	private String[] tags;

	// ****************************************
	// ***            CONSTRUCTOR           ***
	// ****************************************
	
	/**
	 * Initializes a new messages request parameters with all properties initialized with default values.
	 */
	public MessagesRequestParameters ()
	{
		this.setFilterByCreatedDate( false );
		this.setCreatedDateSpan( new DateSpan() );
		this.setFilterByLastModifiedDate( false );
		this.setLastModifiedDateSpan( new DateSpan() );
		this.setFilterByExpirationDate( false );
		this.setExpirationDateSpan( new DateSpan() );
		this.setFilterByTags( false );
		this.setTags( new String[0] );
	}

	/**
	 * Initializes a new messages request parameters with the given values for each properties.
	 * 
	 * @param filterByCreatedDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code createdDate}.
	 * @param createdDateSpan ({@code DateSpan}): Messages created within this date span are included in the query. Otherwise they are excluded.
	 * @param filterByLastModifiedDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code lastModifiedDate}.
	 * @param lastModifiedDateSpan ({@code DateSpan}): Messages lastly modified within this date span are included in the query. Otherwise they are excluded.
	 * @param filterByExpirationDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code expirationDate}.
	 * @param expirationDateSpan ({@code DateSpan}): Messages expiring within this date span are included in the query. Otherwise they are excluded.
	 * @param filterByTags ({@code boolean}): If {@code true} messages to query are filtered using their {@code tags}.
	 * @param tags ({@code String[]}): Messages containing the given tags are included in the query. Otherwise they are excluded.
	 */
	public MessagesRequestParameters (
			boolean filterByCreatedDate,
			DateSpan createdDateSpan,
			boolean filterByLastModifiedDate,
			DateSpan lastModifiedDateSpan,
			boolean filterByExpirationDate,
			DateSpan expirationDateSpan,
			boolean filterByTags,
			String[] tags
			)
	{
		this.setFilterByCreatedDate( filterByCreatedDate );
		this.setCreatedDateSpan( createdDateSpan );
		this.setFilterByLastModifiedDate( filterByLastModifiedDate );
		this.setLastModifiedDateSpan( lastModifiedDateSpan );
		this.setFilterByExpirationDate( filterByExpirationDate );
		this.setExpirationDateSpan(expirationDateSpan );
		this.setFilterByTags( filterByTags );
		this.setTags( tags );
	}

	/**
	 * Initializes a new messages request parameters with the given values for each properties.
	 * 
	 * @param filterByCreatedDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code createdDate}.
	 * @param fromCreatedDate ({@code Date}): Lower bound of the created date span.
	 * @param toCreatedDate ({@code Date}): Upper bound of the created date span.
	 * @param filterByLastModifiedDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code lastModifiedDate}.
	 * @param fromLastModifiedDate ({@code Date}): Lower bound of the last modified date span.
	 * @param toLastModifiedDate ({@code Date}): Upper bound of the last modified date span.
	 * @param filterByExpirationDate ({@code boolean}): If {@code true} messages to query are filtered using their {@code expirationDate}.
	 * @param fromExpirationDate ({@code Date}): Lower bound of the expiration date span.
	 * @param toExpirationDate ({@code Date}): Upper bound of the expiration date span.
	 * @param filterByTags ({@code boolean}): If {@code true} messages to query are filtered using their {@code tags}.
	 * @param tagsList ({@code String}): Comma separated list of tags. Messages containing the given tags are included in the query. Otherwise they are excluded.
	 */
	public MessagesRequestParameters (
			boolean filterByCreatedDate,
			Date fromCreatedDate,
			Date toCreatedDate,
			boolean filterByLastModifiedDate,
			Date fromLastModifiedDate,
			Date toLastModifiedDate,
			boolean filterByExpirationDate,
			Date fromExpirationDate,
			Date toExpirationDate,
			boolean filterByTags,
			String tagsList
			)
	{				
		this.setFilterByCreatedDate( filterByCreatedDate );
		this.setCreatedDateSpan( new DateSpan( fromCreatedDate, toCreatedDate ) );
		this.setFilterByLastModifiedDate( filterByLastModifiedDate );
		this.setLastModifiedDateSpan( new DateSpan( fromLastModifiedDate, toLastModifiedDate ) );
		this.setFilterByExpirationDate( filterByExpirationDate );
		this.setExpirationDateSpan( new DateSpan( fromExpirationDate, toExpirationDate ) );
		this.setFilterByTags( filterByTags );
		this.setTags( tagsList.split(",") );
	}
	
	// ****************************************
	// ***              METHODS             ***
	// ****************************************
	
	/**
	 * Check if at least one filter is enabled.
	 * 
	 * @return Boolean indicating if at least one filter is enabled.
	 */
	public boolean areAnyFilterEnabled ()
	{
		return this.isFilterByCreatedDate()
			|| this.isFilterByLastModifiedDate()
			|| this.isFilterByExpirationDate()
			||this.isFilterByTags();
	}
	
	/**
	 * Check if all filters are disabled.
	 * 
	 * @return Boolean indicating if all filters are disabled.
	 */
	public boolean areAllFiltersDisabled ()
	{
		return !this.areAnyFilterEnabled();
	}
	
	/**
	 * Creates a text representation of this messages request parameters
	 */
	public String toString ()
	{
		return String.format(
				"Filters: {\n\tBy-Created-Date: %b %s,\n\tBy-Last-Modified-Date: %b %s,\n\tBy-Expiration-Date: %b %s,\n\tBy-Tags: %b %s\n\t}",
				this.isFilterByCreatedDate(),
				this.getCreatedDateSpan(),
				this.isFilterByLastModifiedDate(),
				this.getLastModifiedDateSpan(),
				this.isFilterByExpirationDate(),
				this.getExpirationDateSpan(),
				this.isFilterByTags(),
				this.getTags()
				);
	}
	
	/**
	 * Converts this messages request parameters to a MongoDB json query.
	 * 
	 * @return ({@code Query}): MongoDB json query.
	 */
	public Query toQuery ()
	{
		Query query = new Query();
		// Created Date
		if ( this.isFilterByCreatedDate() )
		{
			query.addCriteria(
				Criteria.where( "createdDate" )
					.gte( this.getCreatedDateSpan().getFrom() )
					.lte( this.getCreatedDateSpan().getTo() )
				);
		}
		// Last Modified Date
		if ( this.isFilterByLastModifiedDate() )
		{
			query.addCriteria(
				Criteria.where( "lastModifiedDate" )
					.gte( this.getLastModifiedDateSpan().getFrom() )
					.lte( this.getLastModifiedDateSpan().getTo() )
				);
		}
		// Expiration Date
		if ( this.isFilterByExpirationDate() )
		{
			query.addCriteria(
				Criteria.where( "expirationDate" )
					.gte( this.getExpirationDateSpan().getFrom() )
					.lte( this.getExpirationDateSpan().getTo() )
				);
		}
		// Tags
		if ( this.isFilterByTags() )
		{
			query.addCriteria(
				Criteria.where( "tags" ).all( Arrays.asList( this.getTags() ) )
				);
		}
		return query;
	}

} // class MessagesQuery
