//##################################################
//###                 PACKAGING                  ###
//##################################################

package com.philippepdsr.api.models;

//##################################################
//###                DEPENDANCES                 ###
//##################################################

//Externals
import java.util.Date;
import lombok.Data;

//##################################################
//###               CLASS DATE-SPAN              ###
//##################################################

/**
 * The date span class is used in request body to specify date intervals.
 * 
 * @author Philippe Pérez de San Roman
 */
@Data
public class DateSpan
{	
	
	// ****************************************
	// ***              FIELDS              ***
	// ****************************************
	
	/**
	 * Lower bound of the date span ({@code Date}).
	 */
	private Date from;
	
	/**
	 * Upper bound of the date span ({@code Date}).
	 */
	private Date to;
	
	// ****************************************
	// ***           CONSTRUCTORS           ***
	// ****************************************
	
	public DateSpan ()
	{
		this.setFrom( new Date() );
		this.setTo( new Date() );
	}
	
	public DateSpan( Date from, Date to )
	{
		this.setFrom( from );
		this.setTo( to );
	}

	// ****************************************
	// ***             METHODS              ***
	// ****************************************
	
	public String toString ()
	{
		return String.format( "DateSpan{ from: %s, to: %s }", this.getFrom(), this.getTo() );
	}
	
} // class DateSpan
