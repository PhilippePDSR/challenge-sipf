## JSON Message
```json
{
	"_id": "631fd9fc5e852270801f44e1",
	"createdDate": "2022-01-01T20:12:00.000+00:00",
	"lastModifiedDate": "2022-01-01T20:12:00.000+00:00",
	"text": "mon message",
	"iconUrl": null
	"link": null,
	"tags": [],
	"expirationDate": null,
}
```

## Creating messages:

To create message you can POST a valid json message to the following url.
Leave out the fields 'id', 'createdDate' and 'lastModifiedDate' to have the API generate them for you.
```
POST http://localhost:8080/message
body: {
	text: "some text",
	iconUrl: "https://www.monsite.pf/monimage.png",
	link: "https://www.monsite.pf"
}
```

Optionally you can specify the 'expirationDate':
```
POST http://localhost:8080/message
body: {
	text: "some text",
	iconUrl: "https://www.monsite.pf/monimage.png",
	link: "https://www.monsite.pf",
	expirationDate: "2022-01-01T20:12:00.000+00:00"
}
```

In return you will get a response whoes body contains the created message with the 'id', 'createdDate' and 'lastModifiedDate'.
Something like:
```json
{
	"_id": "631fd9fc5e852270801f44e1",
	"createdDate": "2022-10-05T20:12:14.024+00:00",
	"lastModifiedDate": "2022-10-05T20:12:14.024+00:00",
	"text": "mon message",
	"iconUrl": null
	"link": null,
	"tags": [],
	"expirationDate": null,
}
```

## Read messages:

To read all the messages you can GET the following url.
```
GET http://localhost:8080/messages
```

To read all the message with all the specified tags you can GET the following url and include the tags in json format in the request body.
```
GET http://localhost:8080/messages-with-tags
body: [
	"tag1",
	"tag2",
	...
	tagN"
	]
```

To get all the messages created after a date you can GET the following url and include the date in json format in the request body.
```
GET http://localhost:8080/messages-created-after
body: "2022-09-01"
```

To get all the messages created before a date you can GET the following url and include the date in json format in the request body.
```
GET http://localhost:8080/messages-created-before
body:  "2022-09-01"
```

To get all the messages created between two dates you can GET the following url and include the two dates in json format in the request body.
```
GET http://localhost:8080/messages-created-between
body: {
    "from": "2022-09-01",
    "to": "2022-09-31"
}
```

## Read one message:

To get a single message with a specified id you can GET the following url where ':id' is replaced by the id.
```
GET http://localhost:8080/message/:id
```

## Update messages:

To update a message you can PUT the following url specifying the id, and include the updated message in the body of the request.
You will receive the message back if the request is successfull.
```
PUT http://localhost:8080/message/:id
body: {
	text: "new text",
	iconUrl: "https://www.newsite.pf/newimage.png",
	link: "https://www.newsite.pf"
}
```

## Delete messages:

To delete a message you can DELETE the following url specifying the id.
```
DELETE http://localhost:8080/message/:id
```
