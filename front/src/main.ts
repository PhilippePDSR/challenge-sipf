// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Angulat
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// Application
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// ##################################################
// ###                MAIN PROGRAMM               ###
// ##################################################

// Deploy production mode ?
if ( environment.production )
{
	enableProdMode();
}

// Launch the application
platformBrowserDynamic().bootstrapModule( AppModule )
	.catch( err => console.error(err) );
