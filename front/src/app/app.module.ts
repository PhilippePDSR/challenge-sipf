// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import * as fr from '@angular/common/locales/fr';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Angular Material
import { MatCommonModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';

// Modules
import { AuthentificationModule } from './authentification/authentification.module';

// Routing
import { AppRoutingModule } from './app-routing.module';

// Interceptors
import { AuthentificatonInterceptorProvider } from './authentification/interceptors/authentification-interceptor.interceptor';

// Components
import { AppComponent } from './app.component';

// Pages
import { HomePage } from './home-page.page';

// ##################################################
// ###              CLASS APP-MODULE              ###
// ##################################################

/**
 * Module containing the application.
 */
@NgModule( {
	declarations: [
		AppComponent,
		HomePage
	],
	imports: [
		// Angular
		RouterModule,
		HttpClientModule,
		BrowserModule,
		BrowserAnimationsModule,
		// Angular Material
		MatCommonModule,
		MatButtonModule,
		MatToolbarModule,
		// Routing
		AppRoutingModule,
		// authentification
		AuthentificationModule,
	],
	providers: [
		{ provide: LOCALE_ID, useValue: 'fr-FR' },
		AuthentificatonInterceptorProvider
	],
	bootstrap: [ AppComponent ]
} )
export class AppModule
{

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes the main module of the application.
	 * This sets the default language to french.
	 */
	constructor ()
	{
		registerLocaleData( fr.default );
	}

} // class AppModule
