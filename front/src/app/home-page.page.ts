// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { Component } from '@angular/core';

// ##################################################
// ###               CLASS HOME-PAGE              ###
// ##################################################

/**
 * Home page of the application.
 */
@Component( {
	selector: 'app-home-page',
	templateUrl: './home-page.page.html',
	styleUrls: [ './home-page.page.scss' ]
} )
export class HomePage
{}
