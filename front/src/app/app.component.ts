// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Angular
import { Component } from '@angular/core';

// ##################################################
// ###             CLASS APP-COMPONENT            ###
// ##################################################

/**
 * Main component of the application with the header, vavigation toolbar and rooter outlet.
 */
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent
{}
