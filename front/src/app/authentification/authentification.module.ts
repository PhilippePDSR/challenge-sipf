// Angual
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Material
import { MatCommonModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

// Routing
import { AuthentificationRoutingModule } from './authentification-routing.module';

// Pages
import { LoginPage } from './pages/login-page/login-page.page';


@NgModule( {
    declarations: [
        // Pages
        LoginPage
    ],
    imports: [
        // Angular
        CommonModule,
        // Angular / Material
        MatCommonModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        // Routing
        AuthentificationRoutingModule
    ],
    exports: [
        // Pages
        LoginPage
    ]
} )
export class AuthentificationModule
{}
