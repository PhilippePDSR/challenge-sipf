import { Observable } from 'rxjs';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpHeaders, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthentificationService } from '../services/authentifiaction-service.service';

@Injectable()
export class AuthentificationInterceptor implements HttpInterceptor
{
	private authentificationService : AuthentificationService;

	constructor ( authentificationService : AuthentificationService )
	{
		this.authentificationService = authentificationService;
	}

	intercept ( req : HttpRequest<any>, next : HttpHandler ) : Observable<HttpEvent<any>>
	{
		const token : string = this.authentificationService.getToken();
		const headers = new HttpHeaders().append( 'Authorization', `Bearer ${token}` );
		const modifiedReq = req.clone( { headers: headers } );
		return next.handle( modifiedReq );
	}

} // class AuthentificationInterceptor

export const AuthentificatonInterceptorProvider = [
	{ provide: HTTP_INTERCEPTORS, useClass: AuthentificationInterceptor, multi: true }
];
