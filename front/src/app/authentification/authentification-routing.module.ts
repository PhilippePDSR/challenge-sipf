// Angular
import { NgModule } from '@angular/core';

// Routing
import { RouterModule, Routes } from '@angular/router';

// Pages
import { LoginPage } from './pages/login-page/login-page.page'

const routes : Routes = [
	{ path: 'login', component: LoginPage }
];

@NgModule( {
	imports: [
		RouterModule.forChild( routes )
	],
	exports: [
		RouterModule
	]
} )
export class AuthentificationRoutingModule
{}
