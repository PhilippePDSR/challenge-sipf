import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthentificationService } from '../services/authentifiaction-service.service';

@Injectable( {
    providedIn: 'root'
} )
export class AuthentificationGuard implements CanActivate
{

    private authentificationService : AuthentificationService
    private router : Router;

    constructor ( authentificationService : AuthentificationService, router : Router )
    {
        this.authentificationService = authentificationService;
        this.router = router;
    }

    canActivate ( route : ActivatedRouteSnapshot, state : RouterStateSnapshot ) : boolean
    {
        const token = this.authentificationService.getToken();
        if ( token )
        {
            return true;
        }
        else
        {
            this.router.navigateByUrl( '/login' );
            return false;
        }
    }
}
