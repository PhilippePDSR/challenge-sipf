import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthentificationService
{
    private token! : string;

    login ()
    {
        this.token = 'SIPF_FAKE_TOKEN';
    }

    getToken () : string
    {
        return this.token;
    }
}
