// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Angular
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

// Authentification
import { AuthentificationService } from '../../services/authentifiaction-service.service';

// ##################################################
// ###                LOGIN-PAGE                 ###
// ##################################################

/**
 * Provide an authentification login page.
 */
@Component( {
	selector: 'app-login-page',
	templateUrl: './login-page.page.html',
	styleUrls: [ './login-page.page.scss' ]
} )
export class LoginPage
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * Service used to authenticate the user (AuthentificationService) [injected].
	 */
	private authentificationService: AuthentificationService;

	/**
	 * Location service used to navigate to previous pages (Location) [injected].
	 */
	private location: Location;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new instance of the login page.
	 * 
	 * @param authentificationService (AuthentificationService): Service used to authenticate the user [injected].
	 * @param location (Location) Service used to navigate to previous pages [injected].
	 */
	constructor ( authentificationService: AuthentificationService, router: Router, location: Location )
	{
		this.authentificationService = authentificationService;
		this.location = location;
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Callback invoked when the 'login' button is pressed.
	 */
	onLogin () : void
	{
		this.authentificationService.login();
		this.location.back();
	}

	/**
	 * Callback invoked when the 'cancel' button is pressed.
	 */
	onCancel () : void
	{
		this.location.back();
	}

} // class LoginPageComponent
