// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, } from '@angular/forms';
// Material
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatCommonModule, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

// Rich Text Editor
import { RichTextEditorModule } from '@syncfusion/ej2-angular-richtexteditor';

// Routing
import { MessagesRoutingModule } from './messages-routing.module';

// Components
import { MessagePreviewComponent } from './components/message-preview/message-preview.component'
import { MessageDetailedViewComponent } from './components/message-detailed-view/message-detailed-view.component';
import { MessageFormComponent } from './components/message-form/message-form.component'
import { MessageFilterComponent } from './components/message-filter/message-filter.component'

// Pages
import { MessageCreatePage } from './pages/message-create-page/message-create-page.page';
import { MessageEditPage } from './pages/message-edit-page/message-edit-page.page';
import { MessageDeletePage } from './pages/message-delete-page/message-delete-page.page';
import { MessageDetailsPage } from './pages/message-details-page/message-details-page.page';
import { MessagesListPage } from './pages/messages-list-page/messages-list-page.page';

// Pipes
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

// ##################################################
// ###            CLASS MESSAGES-MODULE           ###
// ##################################################

/**
 * The message module regroups all the components used to interact with messages.
 */
@NgModule({
	declarations: [
		// Components
		MessagePreviewComponent,
		MessageDetailedViewComponent,
		MessageFormComponent,
		MessageFilterComponent,
		// Pages
		MessageCreatePage,
		MessageEditPage,
		MessageDeletePage,
		MessageDetailsPage,
		MessagesListPage,
		// Pipes
		SafeHtmlPipe
	],
	imports: [
		// Angular
		CommonModule,
		ReactiveFormsModule,
		// Angular / Material
		MatButtonModule,
		MatCardModule,
		MatChipsModule,
		MatCommonModule,
		MatDatepickerModule,
		MatDividerModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatNativeDateModule,
		MatSlideToggleModule,
		// Rich Text Editor
		RichTextEditorModule,
		// Routing
		MessagesRoutingModule
	],
	exports: [
		// Angular
		CommonModule,
		ReactiveFormsModule,
		// Angular / Material
		MatDatepickerModule,
		MatFormFieldModule,
		MatNativeDateModule,
		MatIconModule,
		MatInputModule,
		MatChipsModule,
		// Rich Text Editor
		RichTextEditorModule,
		// Routing
		MessagesRoutingModule,
		// Components
		MessagePreviewComponent,
		MessageDetailedViewComponent,
		MessageFormComponent,
		MessageFilterComponent,
		// Pages
		MessageCreatePage,
		MessageEditPage,
		MessageDeletePage,
		MessageDetailsPage,
		MessagesListPage,
		// Pipes
		SafeHtmlPipe
	]
})
export class MessagesModule
{}
