// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, tap } from 'rxjs';

// Models
import { Message } from '../models/message.model';
import { MessagesRequestParameters } from '../models/messages-request-parameters.model'

// ##################################################
// ###           CLASS MESSAGES-SERVICE           ###
// ##################################################

/**
 * Service used to access messages using the REST API.
 *
 * @author Philippe Pérez de San Roman
 */
@Injectable({
	providedIn: 'root'
})
export class MessagesService
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * HttpClient used to access the api.
	 */
	private api: HttpClient;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new instance of the MessagesService with the given HttpClient.
	 * 
	 * @param api (HttpClient): Client used to access the api.
	 */
	constructor ( api: HttpClient )
	{
		this.api = api;
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Queries all the messages from the API.
	 *
	 * @returns (Observable<Message[]>):  Observable returning arrays of messages.
	 */
	getAllMessages ( messagesRequestParameters: MessagesRequestParameters ): Observable<Message[]>
	{
		return this.api.get<Message[]>(
			'http://localhost:8080/messages',
			{ params: messagesRequestParameters.toHttpParameters() }
			);
	}

	/**
	 * Query a single message using its id.
	 * 
	 * @param id (@type{string}): Unique identifier of the message.
	 * @returns (Observable<Message>): Observable returning a single message.
	 */
	getMessage ( id: string ): Observable<Message>
	{
		return this.api.get<Message>( 'http://localhost:8080/message/' + id );
	}

	/**
	 * Create a new message.
	 * 
	 * @param message (Message): Message to create.
	 * @returns (Observable<Message>): Observable returning a single message.
	 */
	createMessage ( message: Message ): Observable<Message>
	{
		return this.api.post<Message>( 'http://localhost:8080/message', message );
	}

	/**
	 * Update a single message.
	 * 
	 * @param message (Message): Message to update.
	 * @returns (Observable<Message>): Observable returning the updated message.
	 */
	updateMessage ( message: Message ) : Observable<Message>
	{
		return this.api.put<Message>( 'http://localhost:8080/message/' + message.id, message );
	}

	/**
	 * Delete a single message.
	 * 
	 * @param id (@type{string}): Unique identifier of the message.
	 * @returns (Observable<string>): Observable returning the delete response.
	 */
	deleteMessage ( id: string ) : Observable<string>
	{
		return this.api.delete<string>( 'http://localhost:8080/message/' + id );
	}

} //  class MessagesService
