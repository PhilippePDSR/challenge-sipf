// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// RxJs
import { tap } from 'rxjs';

// Angular
import { Component } from '@angular/core';
import { Router } from '@angular/router';

// Models
import { Message } from '../../models/message.model';

// Services
import { MessagesService } from '../../services/messages-service.service';

// ##################################################
// ###         CLASS MESSAGE-CREATE-PAGE          ###
// ##################################################

/**
 * Page used to create new messages.
 *
 * @autor Philippe Pérez de San Roman
 */
@Component( {
	selector: 'message-create-page',
	templateUrl: './message-create-page.page.html',
	styleUrls: [ './message-create-page.page.scss' ]
} )
export class MessageCreatePage
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * Router used to navigate in the application using urls (Router) [injected].
	 */
	private router: Router;

	/**
	 * Service used to access messages using the HTTP API (MessagesService) [injected].
	 */
	private messagesService: MessagesService;

	/*
	 * Local copy of the created message used to initialize the form (Message).
	 */
	public message: Message = new Message();

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new message create page with the given injectables.
	 * 
	 * @param messagesService (MessagesService): Service used to access messages using the HTTP API [injected].
	 * @param router (Router): Router used to navigate in the application using urls [injected]
	 */
	constructor ( messagesService: MessagesService, router: Router )
	{
		this.router = router;
		this.messagesService = messagesService;
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Callback invoked when the 'accept' dialog button is clicked.
	 * This creates a new message.
	 */
	onAcceptClicked ( message: Message ): void
	{
		this.messagesService.createMessage( message ).pipe(
			tap( () => this.router.navigateByUrl( '/messages' ) )
		).subscribe();
	}

	/**
	 * Callback invoked when the 'cancel' dialog button is clicked.
	 * The application then loads the '/messages' url.
	 */
	onCancelClicked ()
	{
		this.router.navigateByUrl( '/messages' );
	}

} // class MessageCreatePage
