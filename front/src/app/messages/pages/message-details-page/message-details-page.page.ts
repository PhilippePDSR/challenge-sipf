// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Models
import { Message } from '../../models/message.model';

// Services
import { MessagesService } from '../../services/messages-service.service';

// Pages
import { MessagePage } from '../message-page/message-page.page';

// ##################################################
// ###         CLASS MESSAGE-DETAILS-PAGE         ###
// ##################################################
/**
 * Page used to view a message in details using its 'id' given as route parameter.
 *
 * @autor Philippe Pérez de San Roman
 */
@Component( {
	selector: 'message-details-page',
	templateUrl: './message-details-page.page.html',
	styleUrls: [ './message-details-page.page.scss' ]
} )
export class MessageDetailsPage extends MessagePage
{

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new message details page with the given injectables.
	 * 
	 * @param router (Router): Router used to navigate in the application using urls [injected].
	 * @param activatedRoute (ActivatedRoute): Activated route used to query route parameters [injected].
	 * @param messagesServices (MessagesService): Service used to access messages using the HTTP API [injected].
	 */
	constructor ( router: Router, activatedRoute: ActivatedRoute, messagesServices: MessagesService )
	{
		super( router, activatedRoute, messagesServices );
	}

} // class MessageDetailsPage
