// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Models
import { Message } from '../../models/message.model';

// Services
import { MessagesService } from '../../services/messages-service.service';

// ##################################################
// ###             CLASS MESSAGE-PAGE             ###
// ##################################################

/**
 * Base class for message edition pages.
 *
 * @autor Philippe Pérez de San Roman
 */
@Component( {
	selector: 'message-page',
	templateUrl: './message-page.page.html',
	styleUrls: [ './message-page.page.scss' ]
} )
export class MessagePage implements OnInit, AfterViewInit
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * Router used to navigate in the application using urls (Router) [injected].
	 */
	protected router: Router;

	/**
	 * Activated route used to query route parameters (ActivatedRoute) [injected].
	 */
	protected activatedRoute: ActivatedRoute;

	/**
	 * Service used to access messages using the HTTP API (MessagesService) [injected].
	 */
	protected messagesServices: MessagesService;

	/**
	 * Local copy of the message to delete (Message).
	 */
	public message!: Message;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new message delete page with the given injectables.
	 * 
	 * @param router (Router): Router used to navigate in the application using urls [injected].
	 * @param activatedRoute (ActivatedRoute): Activated route used to query route parameters [injected].
	 * @param messagesServices (MessagesService): Service used to access messages using the HTTP API [injected].
	 */
	constructor ( router: Router, activatedRoute: ActivatedRoute, messagesServices: MessagesService )
	{
		this.router = router;
		this.activatedRoute = activatedRoute;
		this.messagesServices = messagesServices;
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Life-cycle method called uppon initialization of the page.
	 * Used to retreive the 'id' of the message from the route parameters
	 * and load it using the message service.
	 */
	ngOnInit (): void
	{
		this.parseIdAndLoadMessage();
	}

	/**
	 * Life-cycle method called after the view was initialized.
	 */
	ngAfterViewInit (): void
	{}

	/**
	 * Read and parses the message id from the route parameters and loads the message.
	 */
	parseIdAndLoadMessage (): void
	{
		this.activatedRoute.paramMap.subscribe(
			params => this.loadMessage( params.get( 'id' ) )
		);
	}

	/**
	 * Load the message with the given id.
	 * 
	 * @param id (string|null): Unique identifier of the message.
	 */
	loadMessage ( id: string | null ): void
	{
		// Cancel loading if the id is null
		if ( id == null )
		{
			return;
		}

		// Otherwise, attemps to get it from the message service
		this.messagesServices.getMessage( id ).subscribe(
			// Assign the loaded message to the preview
			response => { this.message = response; }
		);
	}

	/**
	 * Callback invoked when the 'accept' dialog button is clicked.
	 * The application then loads the '/messages' url.
	 */
	onAcceptClicked ( message: Message ): void
	{
		this.router.navigateByUrl( '/messages' );
	}

	/**
	 * Callback invoked when the 'cancel' dialog button is clicked.
	 * The application then loads the '/messages' url.
	 */
	onCancelClicked ()
	{
		this.router.navigateByUrl( '/messages' );
	}

} // class MessageDeleteComponent
