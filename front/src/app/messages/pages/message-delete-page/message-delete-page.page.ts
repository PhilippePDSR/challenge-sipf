// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Models
import { Message } from '../../models/message.model';

// Services
import { MessagesService } from '../../services/messages-service.service';

// Pages
import { MessagePage } from '../message-page/message-page.page';

// ##################################################
// ###         CLASS MESSAGE-DELETE-PAGE          ###
// ##################################################
/**
 * Page used to delete a message using its 'id' given as route parameter.
 *
 * @autor Philippe Pérez de San Roman
 */
@Component( {
	selector: 'message-delete-page',
	templateUrl: './message-delete-page.page.html',
	styleUrls: [ './message-delete-page.page.scss' ]
} )
export class MessageDeletePage extends MessagePage
{

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new message delete page with the given injectables.
	 * 
	 * @param router (Router): Router used to navigate in the application using urls [injected].
	 * @param activatedRoute (ActivatedRoute): Activated route used to query route parameters [injected].
	 * @param messagesServices (MessagesService): Service used to access messages using the HTTP API [injected].
	 */
	constructor ( router: Router, activatedRoute: ActivatedRoute, messagesServices: MessagesService )
	{
		super( router, activatedRoute, messagesServices );
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Callback invoked when the 'accept' dialog button is clicked.
	 * The message id is sent to the API to delete the message.
	 * The application then loads the '/messages' url.
	 */
	override onAcceptClicked ( message: Message ): void
	{
		this.messagesServices.deleteMessage( this.message.id ).subscribe(
			() => super.onAcceptClicked( message )
		);

	}

} // class MessageDeletePage
