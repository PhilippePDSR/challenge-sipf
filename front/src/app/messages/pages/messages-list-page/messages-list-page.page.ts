// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// RXJS
import { Observable } from 'rxjs';

// Agular
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Agular Material

// Models
import { MessageArray } from '../../models/message.model';

// Services
import { MessagesService } from '../../services/messages-service.service';
import { MessagesRequestParameters } from '../../models/messages-request-parameters.model';

// ##################################################
// ###           CLASS MESSAGE-LIST-PAGE          ###
// ##################################################

/**
 * Page used to display the list of messages.
 */
@Component( {
	selector: 'messages-list-page',
	templateUrl: './messages-list-page.page.html',
	styleUrls: [ './messages-list-page.page.scss' ]
} )
export class MessagesListPage implements OnInit
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * Router used to navigate in the application using urls (Router) [injected].
	 */
	protected router: Router;

	/**
	 * Service used to access messages using the HTTP API (MessagesService) [injected].
	 */
	protected messagesServices: MessagesService;

	/**
	 * Observable used to access the messages (Observable<MessageArray>).
	 */
	protected messages$!: Observable<MessageArray>;

	/**
	 * Local copy of the messages request parameters (MessagesRequestParameters).
	 */ 
	protected messagesRequestParameters: MessagesRequestParameters;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new instance of the messages list page class.
	 * 
	 * @param messagesServices (MessagesService): Service used to access messages using the HTTP API [injected].
	 * @param router (Router): Router used to navigate in the application using urls [injected].
	 */
	constructor ( messagesServices: MessagesService, router: Router )
	{
		this.messagesServices = messagesServices;
		this.router = router;
		this.messagesRequestParameters = MessagesRequestParameters.default();
	}

	/**
	 * Life-cycle method called uppon initialization of the page.
	 * Used to make the query to load the messages.
	 */
	ngOnInit (): void
	{
		this.messages$ = this.messagesServices.getAllMessages( this.messagesRequestParameters );
	}

	/**
	 * Callback invoked when the create button at the bottom of the list is clicked.
	 */
	onButtonAddClicked (): void
	{
		this.router.navigateByUrl( "/messages/create" );
	}

	/**
	 * Callback invoked when new filters must be applied.
	 * 
	 * @param messagesRequestParameters (MessagesRequestParameters): Messages request parameters describing the filters.
	 */
	onApplyFilters ( messagesRequestParameters: MessagesRequestParameters ): void
	{
		this.messagesRequestParameters = messagesRequestParameters;
		this.messages$ = this.messagesServices.getAllMessages( this.messagesRequestParameters );
	}

} // class MessagesListPage
