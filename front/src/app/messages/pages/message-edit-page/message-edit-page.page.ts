// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Components
import { MessageFormComponent } from '../../components/message-form/message-form.component';

// Model
import { Message } from '../../models/message.model';

// Services
import { MessagesService } from '../../services/messages-service.service';

// Pages
import { MessagePage } from '../message-page/message-page.page';

// ##################################################
// ###       CLASS MESSAGE-EDIT-COMPONENT       ###
// ##################################################

/**
 * Component used to edit a message using its 'id' given as route parameter.
 *
 * @autor Philippe Pérez de San Roman
 */
@Component( {
	selector: 'message-edit-page',
	templateUrl: './message-edit-page.page.html',
	styleUrls: [ './message-edit-page.page.scss' ]
} )
export class MessageEditPage extends MessagePage implements AfterViewInit
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/*
	 * Local reference to the child message form component (MessageFormComponent).
	 */
	@ViewChild( MessageFormComponent )
	public messageForm!: MessageFormComponent;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new message delete component with the given injectables.
	 * 
	 * @param router (Router): Router used to navigate the application.
	 * @param activatedRoute (ActivatedRoute): Activate route informations used to retreive parameters.
	 * @param messagesServices (MessagesService): Used to access the API through HTTP.
	 */
	constructor ( router: Router, activatedRoute: ActivatedRoute, messagesServices: MessagesService )
	{
		super( router, activatedRoute, messagesServices );
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Life-cycle method called uppon initialization of the page.
	 */
	override ngOnInit (): void
	{}

	/**
	 * Life-cycle method called after the view was initialized.
	 * Here we load the message with the input 'id' and set it in the form and preview.
	 */
	override ngAfterViewInit (): void
	{
		this.parseIdAndLoadMessage();
	}

	/**
	 * Load the message with the given id.
	 * And set it in the form and preview.
	 * 
	 * @param id (string|null): Unique identifier of the message.
	 */
	override loadMessage ( id: string | null ): void
	{
		super.loadMessage( id );
	}

	/**
	 * Callback invoked when the confirm dialog button is clicked.
	 * The message is sent to the API to be updated.
	 * The application then loads the '/messages' url.
	 */
	override onAcceptClicked ( message: Message ): void
	{
		message = this.messageForm.message;

		this.messagesServices.updateMessage( message ).subscribe(
			() => super.onAcceptClicked( message )
		);
	}

} // class MessageEditPage
