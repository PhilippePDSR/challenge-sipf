// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// RxJs
import { Observable, map, tap } from 'rxjs';

// Angular
import { AfterViewInit, Component, Input, Output, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

// Angular Material
import { MatChipInputEvent } from '@angular/material/chips';

// Rich Text Editor
import { ToolbarService, HtmlEditorService, RichTextEditor, ChangeEventArgs } from '@syncfusion/ej2-angular-richtexteditor';

// Models
import { Message } from '../../models/message.model';
import { MessagePreviewComponent } from '../message-preview/message-preview.component';

// ##################################################
// ###        CLASS MESSAGE-FORM-COMPONENT        ###
// ##################################################

/**
 * Title, form and dialog buttons used to create, edit and delete messages.
 */
@Component( {
	selector: 'message-form',
	templateUrl: './message-form.component.html',
	styleUrls: [ './message-form.component.scss' ],
	providers: [ ToolbarService, HtmlEditorService ]
} )
export class MessageFormComponent implements OnInit, AfterViewInit
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/*
	 * Form builder used to create the reactive form (FormBuilder) [injected].
	 */
	private formBuilder: FormBuilder;

	/**
	 * Form groud binded to the html form to create the reactive form (FormGroup).
	 */
	public form!: FormGroup;

	/*
	 * Input message (Message).
	 */
	@Input()
	message!: Message;

	/**
	 * Can the message be modified ?
	 */
	@Input()
	editable: 'true'|'false' = 'true';

	/*
	 * Input title displayed in the header (string).
	 */
	@Input()
	title! : string;

	/*
	 * Output event emitted when the 'accept' button is clicked.
	 * This event emmits the current state of the message.
	 */
	@Output()
	accept = new EventEmitter<Message>();

	/*
	 * Output event emitted when the 'cancel' button is clicked.
	 */
	@Output()
	cancel = new EventEmitter();

	@ViewChild( MessagePreviewComponent )
	public preview!: MessagePreviewComponent;

	@ViewChild( RichTextEditor )
	public richTextEditor!: RichTextEditor;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initializes a new instance of the message form component.
	 * 
	 * @param formBuilder (FormBuilder): Form builder used to create the reactive form [injected].
	 */
	constructor ( formBuilder: FormBuilder )
	{
		this.formBuilder = formBuilder;
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Life-cycle method called uppon initialization of the component.
	 * Used to create the reactive form and the message preview observable
	 * and load it using the message service.
	 */
	ngOnInit (): void
	{
		// Create the reactive form
		this.form = this.formBuilder.group( {
			text: new FormControl( this.message ? this.message.text : '', Validators.required ),
			iconUrl: new FormControl( this.message ? this.message.iconUrl : '' ),
			link: new FormControl( this.message ? this.message.link : '' ),
			expirationDate: new FormControl( this.message ? this.message.expirationDate : null ),
			tags: new FormControl( this.message ? this.message.tags : [] )
		} );

		this.form.valueChanges.pipe(
			tap( value => this.message = {
				id: this.message ? this.message.id : '',
				createdDate: this.message ? this.message.createdDate : new Date(),
				lastModifiedDate: this.message ? this.message.lastModifiedDate : new Date(),
				tags: this.message ? this.message.tags : [],
				...value // text, iconUrl, link, expirationDate
			} )
			).subscribe();
	}

	ngAfterViewInit (): void
	{}

	// **************************************************
	// ***                  CALLBACKS                 ***
	// **************************************************

	onRichTextChanged ( changeEvent: ChangeEventArgs ): void
	{
		const control = this.form.get( 'text' );
		const text = changeEvent.value;

		// Add the new tag
		if ( control !== null && text ) {
			control.setValue( text );
			control.updateValueAndValidity();
			this.message.text = text;
		}
	}

	/**
	 * Callback invoked when a tag is added.
	 * 
	 * @param event (MatChipInputEvent): Event containing the tag.
	 */
	onAddTag ( event: MatChipInputEvent ): void
	{
		const control = this.form.get( 'tags' );
		const tag = ( event.value || '' ).trim();

		// Add the new tag
		if ( control !== null && tag )
		{
			control.value.push( tag );
			control.updateValueAndValidity();
		}

		// Clear the input value
		event.chipInput!.clear();
	}

	/**
	 * Callback invoked when a tag is removed.
	 * 
	 * @param tag (string): Tag to remove.
	 */
	onRemoveTag ( tag: string ): void
	{
		const control = this.form.get( 'tags' );

		if ( control !== null ) 
		{
			const index: number = control?.value.indexOf( tag, 0 );
			control?.value.splice( index, 1 );
			control?.updateValueAndValidity();
		}
	}

	/**
	 * Callback invoked when the 'accept' dialog button is clicked.
	 * This update the internal message and emit the 'accept' event with it.
	 */
	onAcceptClicked ()
	{
		this.message = {
			id:               this.message !== null ? this.message.id : '',
			createdDate:      this.message !== null ? this.message.createdDate : Date.now(),
			lastModifiedDate: this.message !== null ? this.message.lastModifiedDate : Date.now(),
			tags:             this.message !== null ? this.message.tags : [],
			...this.form.value // text, iconUrl, link, expirationDate
		};

		this.accept.emit( this.message );
	}

	/**
	 * Callback invoked when the 'cancel' dialog button is clicked.
	 * This method emits the 'cancel' event.
	 */
	onCancelClicked ()
	{
		this.cancel.emit();
	}

} // class MessageFormComponent
