// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

// Models
import { Message } from '../../models/message.model';

// ##################################################
// ###      CLASS MESSAGES-PREVIEW-COMPONENT      ###
// ##################################################

/**
 * Displays a message without the details.
 */
@Component( {
	selector: 'message-preview',
	templateUrl: './message-preview.component.html',
	styleUrls: [ './message-preview.component.scss' ]
} )
export class MessagePreviewComponent
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * Router used to navigate in the application using urls (Router) [injected].
	 */
	private router : Router;

	/**
	 * Input message (Message).
	 */
	@Input()
	public message! : Message;

	/**
	 * Display the details button ? ('true' | 'false')
	 */
	@Input()
	public showDetailsButton!: 'true'|'false';

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initialize a new instance of the message preview component.
	 * 
	 * @param router (Router): Router used to navigate in the application using urls [injected].
	 */
	constructor ( router: Router )
	{
		this.router = router;
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Callback invoked when the 'details' button is clicked.
	 */
	onViewDetailsClicked (): void
	{
		this.router.navigateByUrl( '/messages/details/' + this.message.id );
	}

} // class MessagePreviewComponent
