// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Agular
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

// Models
import { Message } from '../../models/message.model';

// ##################################################
// ###   CLASS MESSAGES-DETAILED-VIEW-COMPONENT   ###
// ##################################################

/**
 * Displays a message with all the details the details.
 */
@Component( {
	selector: 'message-detailed-view',
	templateUrl: './message-detailed-view.component.html',
	styleUrls: [ './message-detailed-view.component.scss' ]
} )
export class MessageDetailedViewComponent
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * Router used to navigate in the application using urls (Router) [injected].
	 */
	private router: Router;

	/**
	 * Input message (Message).
	 */
	@Input()
	public message!: Message;

	/**
	 * Input used to enable or diabled the presence of the 'edit' and 'delete' buttons.
	 */
	@Input()
	public showEditButton: 'true' | 'false' = 'true';

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initialize a new instance of the message full view component.
	 * 
	 * @param router (Router): Router used to navigate in the application using urls [injected].
	 */
	constructor ( router: Router )
	{
		this.router = router;
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	/**
	 * Callback invoked when the 'edit' button is clicked.
	 */
	onEditMessage (): void
	{
		this.router.navigateByUrl(
			'/messages/edit/' + this.message.id
		);
	}

	/**
	 * Callback invoked when the 'delete' button is clicked.
	 */
	onDeleteMessage (): void
	{
		this.router.navigateByUrl(
			'/messages/delete/' + this.message.id
		);
	}

} // class MessageDetailedViewComponent
