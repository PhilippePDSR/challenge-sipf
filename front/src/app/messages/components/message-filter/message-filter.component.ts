// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// RxJs
import { Observable, map, tap } from 'rxjs';

// Angular
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

// Angular Material
import { MatChipInputEvent } from '@angular/material/chips';

// Models
import { MessagesRequestParameters } from '../../models/messages-request-parameters.model';
import { DateSpan } from '../../models/date-span.model';

// ##################################################
// ###       CLASS MESSAGE-FILTER-COMPONENT       ###
// ##################################################

/**
 * Title, form and dialog buttons used to create, edit and delete messages.
 */
@Component( {
	selector: 'message-filter',
	templateUrl: './message-filter.component.html',
	styleUrls: [ './message-filter.component.scss' ]
} )
export class MessageFilterComponent implements OnInit
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * Event emitted when the apply button is clicked (EventEmitter<MessagesRequestParameters>).
	 * The event carries the messages request parameters.
	 */
	@Output()
	public apply: EventEmitter<MessagesRequestParameters> = new EventEmitter<MessagesRequestParameters>();

	/**
	 * Local copy of the messages request parameters (MessagesRequestParameters).
	 * They are updates when the form is edited using the valueChange observable.
	 */
	protected messagesRequestParameters: MessagesRequestParameters;

	/**
	 * Reactive form binded to the HTML form (FormGroup).
	 */
	protected form!: FormGroup;

	/*
	 * Form builder used to create the ractive form (FormBuilder) [injected].
	 */
	private formBuilder: FormBuilder;

	protected fromRefValue!: any;
	protected formChanged: boolean;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	constructor ( formBuilder: FormBuilder )
	{
		this.formBuilder = formBuilder;
		this.messagesRequestParameters = MessagesRequestParameters.default();
		this.formChanged = false;
	}

	// **************************************************
	// ***                  METHODS                   ***
	// **************************************************

	ngOnInit (): void
	{
		this.form = this.formBuilder.group({
			filterByCreatedDate: new FormControl<boolean>( false ),
			fromCreatedDate: new FormControl<Date|null>( null ),
			toCreatedDate: new FormControl<Date|null>( null ),
			filterByLastModifiedDate: new FormControl<boolean>( false ),
			fromLastModifiedDate: new FormControl<Date|null>( null ),
			toLastModifiedDate: new FormControl<Date | null>( null ),
			filterByExpirationDate: new FormControl<boolean>( false ),
			fromExpirationDate: new FormControl<Date | null>( null ),
			toExpirationDate: new FormControl<Date | null>( null ),
			filterByTags: new FormControl<boolean>( false ),
			tags: new FormControl<string[]>( [] )
		})
		this.form.controls[ 'fromCreatedDate' ].disable();
		this.form.controls[ 'toCreatedDate' ].disable();
		this.form.controls[ 'fromLastModifiedDate' ].disable();
		this.form.controls[ 'toLastModifiedDate' ].disable();
		this.form.controls[ 'fromExpirationDate' ].disable();
		this.form.controls[ 'toExpirationDate' ].disable();
		this.form.controls[ 'tags' ].disable();

		this.form.valueChanges.pipe(
			tap( value => this.updateMessagesRequestParameters( value ) ),
		).subscribe();

		this.fromRefValue = this.form.value;
		this.form.valueChanges.subscribe( value =>
		{
			this.formChanged = Object.keys( this.fromRefValue ).some( key => this.form.value[ key ] != this.fromRefValue[ key ] );
		} );
	}

	updateMessagesRequestParameters ( value: any ): void
	{
		this.messagesRequestParameters = MessagesRequestParameters.fromAny( value );
	}

	// **************************************************
	// ***                  CALLBACKS                 ***
	// **************************************************


	/**
	 * Callback invoked when the filter by created date toggle is toggled.
	 */
	onToggleFilterByCreatedDate (): void
	{
		if ( this.form.controls['fromCreatedDate'].disabled )
		{
			this.form.controls['fromCreatedDate'].enable();
			this.form.controls['toCreatedDate'].enable();
		}
		else
		{
			this.form.controls['fromCreatedDate'].disable();
			this.form.controls['toCreatedDate'].disable();
		}
	}

	/**
	 * Callback invoked when the filter by last modified date toggle is toggled.
	 */
	onToggleFilterByLastModifiedDate (): void
	{
		if ( this.form.controls['fromLastModifiedDate'].disabled )
		{
			this.form.controls['fromLastModifiedDate'].enable();
			this.form.controls['toLastModifiedDate'].enable();
		}
		else
		{
			this.form.controls['fromLastModifiedDate'].disable();
			this.form.controls['toLastModifiedDate'].disable();
		}
	}

	/**
	 * Callback invoked when the filter by expiration date toggle is toggled.
	 */
	onToggleFilterByExpirationDate (): void
	{
		if ( this.form.controls[ 'fromExpirationDate' ].disabled )
		{
			this.form.controls[ 'fromExpirationDate' ].enable();
			this.form.controls[ 'toExpirationDate' ].enable();
		}
		else
		{
			this.form.controls[ 'fromExpirationDate' ].disable();
			this.form.controls[ 'toExpirationDate' ].disable();
		}
	}

	/**
	 * Callback invoked when the filter by tag toggle is toggled.
	 */
	onToggleFilterByTags (): void
	{
		if ( this.form.controls['tags'].disabled )
		{
			this.form.controls['tags'].enable();
		}
		else
		{
			this.form.controls['tags'].disable();
		}
	}

	/**
	 * Callback invoked when a tag is added.
	 * 
	 * @param event (MatChipInputEvent): Event containing the tag.
	 */
	onAddTag ( event: MatChipInputEvent ): void
	{
		const control = this.form.get( 'tags' );
		const tag = ( event.value || '' ).trim();

		// Add the new tag
		if ( control !== null && tag )
		{
			control?.value.push( tag );
			control?.updateValueAndValidity();
		}

		// Clear the input value
		event.chipInput!.clear();
	}

	/**
	 * Callback invoked when a tag is removed.
	 * 
	 * @param tag (string): Tag to remove.
	 */
	onRemoveTag ( tag: string ): void
	{
		const control = this.form.get( 'tags' );

		if ( control !== null ) 
		{
			const index: number = control?.value.indexOf( tag, 0 );
			control?.value.splice( index, 1 );
			control?.updateValueAndValidity();
		}
	}

	/**
	 * Callback invoked when aplly button is clicked.
	 */
	onApplyClicked ()
	{
		this.apply.emit( this.messagesRequestParameters );
		this.fromRefValue = this.form.value;
		this.formChanged = false;
	}

} // class MessageFilterComponent
