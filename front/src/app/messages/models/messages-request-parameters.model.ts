// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Angular
import { HttpParams } from '@angular/common/http';

// Model
import { DateSpan } from './date-span.model';

// ##################################################
// ###      CLASS MESSAGES-REQUEST-PARAMETERS     ###
// ##################################################

export class MessagesRequestParameters
{

	// ****************************************
	// ***              FIELDS              ***
	// ****************************************

	/**
	 * If true messages to query are filtered using their createdDate.
	 * In this case the createdDateSpan property must also be specified.
	 */
	public filterByCreatedDate: boolean;

	/**
	 * Messages created within this date span are included in the query.
	 * Otherwise they are excluded.
	 */
	public createdDateSpan: DateSpan;

	/**
	 * If true messages to query are filtered using their lastModifiedDate.
	 * In this case the lastModifiedDateSpan property must also be specified.
	 */
	public filterByLastModifiedDate: boolean;

	/**
	 * Messages lastly modified within this date span are included in the query.
	 * Otherwise they are excluded.
	 */
	public lastModifiedDateSpan: DateSpan;

	/**
	 * If true messages to query are filtered using their expirationDate.
	 * In this case the expirationDateSpan property must also be specified.
	 */
	public filterByExpirationDate: boolean;

	/**
	 * Messages expiring within this date span are included in the query.
	 * Otherwise they are excluded.
	 */
	public expirationDateSpan: DateSpan;

	/**
	 * If true messages to query are filtered using their tags.
	 * In this case the tags property must also be specified.
	 */
	public filterByTags: boolean;

	/**
	 * Messages containing the given tags are included in the query.
	 * Otherwise they are excluded.
	 */
	public tags: string[];

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	constructor (
		filterByCreatedDate: boolean,
		createdDateSpan: DateSpan,
		filterByLastModifiedDate: boolean,
		lastModifiedDateSpan: DateSpan,
		filterByExpirationDate: boolean,
		expirationDateSpan: DateSpan,
		filterByTags: boolean,
		tags: string[]
	)
	{
		this.filterByCreatedDate = filterByCreatedDate;
		this.createdDateSpan = createdDateSpan;
		this.filterByLastModifiedDate = filterByLastModifiedDate;
		this.lastModifiedDateSpan = lastModifiedDateSpan;
		this.filterByExpirationDate = filterByExpirationDate;
		this.expirationDateSpan = expirationDateSpan;
		this.filterByTags = filterByTags;
		this.tags = tags;
	}

	// **************************************************
	// ***                   GETTERS                  ***
	// **************************************************

	public get areAnyFiltersEnabled (): boolean
	{
		return this.filterByCreatedDate
			|| this.filterByLastModifiedDate
			|| this.filterByExpirationDate
			|| this.filterByTags;
	}

	public get areAllFiltersDisabled (): boolean
	{
		return !this.areAnyFiltersEnabled;
	}

	public get tagsAsACommaSeparatedString (): string
	{
		return this.tags.join( ',' );
	}

	// **************************************************
	// ***                   METHODS                  ***
	// **************************************************

	public toHttpParameters (): HttpParams
	{
		return new HttpParams( {
			fromObject: { ...this.toObject() }
		} );
	}

	public toObject (): object
	{
		return {
			'filterByCreatedDate': this.filterByCreatedDate,
			'fromCreatedDate': this.createdDateSpan.from.toUTCString(),
			'toCreatedDate': this.createdDateSpan.to.toUTCString(),
			'filterByLastModifiedDate': this.filterByLastModifiedDate,
			'fromLastModifiedDate': this.lastModifiedDateSpan.from.toUTCString(),
			'toLastModifiedDate': this.lastModifiedDateSpan.to.toUTCString(),
			'filterByExpirationDate': this.filterByExpirationDate,
			'fromExpirationDate': this.expirationDateSpan.from.toUTCString(),
			'toExpirationDate': this.expirationDateSpan.to.toUTCString(),
			'filterByTags': this.filterByTags,
			'tagsList': this.tagsAsACommaSeparatedString
		};
	}

	public toString (): string
	{
		return "MessagesRequestParameters {\n"
			+ "\tfilterByCreatedDate: " + this.filterByCreatedDate + ",\n"
			+ "\tcreatedDateSpan " + this.createdDateSpan + ",\n"
			+ "\tfilterByLastModifiedDate: " + this.filterByLastModifiedDate + ",\n"
			+ "\tlastModifiedDateSpan " + this.lastModifiedDateSpan + ",\n"
			+ "\tfilterByExpirationDate: " + this.filterByExpirationDate + ",\n"
			+ "\texpirationDateSpan " + this.expirationDateSpan + ",\n"
			+ "\tfilterByTags: " + this.filterByTags + ",\n"
			+ "\ttags: " + (this.tags ? this.tagsAsACommaSeparatedString : "[]") + "\n"
			+ "\t}";
	}

	// **************************************************
	// ***               STATIC-METHODS               ***
	// **************************************************

	public static default (): MessagesRequestParameters
	{
		let defaultDate: Date = new Date( Date.now() );
		let defaultDateSpan: DateSpan = new DateSpan( defaultDate, defaultDate );
		return new MessagesRequestParameters( false, defaultDateSpan, false, defaultDateSpan, false, defaultDateSpan, false, [] );
	}

	public static fromAny ( value: any ): MessagesRequestParameters
	{
		let defaultDate: Date = new Date( Date.now() );
		let params = MessagesRequestParameters.default()

		// Created Date
		if ( value[ 'filterByCreatedDate' ] === true ) {
			params.filterByCreatedDate = true;
			params.createdDateSpan = new DateSpan(
				value[ 'fromCreatedDate' ] ? value[ 'fromCreatedDate' ] : defaultDate,
				value[ 'toCreatedDate' ] ? value[ 'toCreatedDate' ] : defaultDate,
			);
		}

		// Last Modified Date
		if ( value[ 'filterByLastModifiedDate' ] === true ) {
			params.filterByLastModifiedDate = true;
			params.lastModifiedDateSpan = new DateSpan(
				value[ 'fromLastModifiedDate' ] ? value[ 'fromLastModifiedDate' ] : defaultDate,
				value[ 'toLastModifiedDate' ] ? value[ 'toLastModifiedDate' ] : defaultDate,
			);
		}

		// Expiration Date
		if ( value[ 'filterByExpirationDate' ] === true ) {
			params.filterByExpirationDate = true;
			params.expirationDateSpan = new DateSpan(
				value[ 'fromExpirationDate' ] ? value[ 'fromExpirationDate' ] : defaultDate,
				value[ 'toExpirationDate' ] ? value[ 'toExpirationDate' ] : defaultDate,
			);
		}

		// Tags
		if ( value[ 'filterByTags' ] === true ) {
			params.filterByTags = true;
			params.tags = value[ 'tags' ];
		}

		return params;
	}
}
