// ##################################################
// ###              CLASS DATE-SPAN               ###
// ##################################################

export class DateSpan
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	public from: Date;
	public to: Date;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	constructor ( from: Date, to: Date )
	{
		this.from = new Date( from );
		this.to   = new Date( to );
	}

	public copy (): DateSpan
	{
		return new DateSpan( this.from, this.to )
	}

	public toString (): string
	{
		return "DateSpan: { from: " + this.from.toUTCString() + ", to: " + this.to.toUTCString() + " }"
	}

} // class DateSpan
