// ##################################################
// ###               CLASS MESSAGE                ###
// ##################################################

/**
 * Messages displayed in this application.
 */
export class Message
{

	// **************************************************
	// ***                   FIELDS                   ***
	// **************************************************

	/**
	 * Unique identifier of the message (string).
	 */
	public id: string;

	/**
	 * Date and time when the message was first created (Date).
	 */
	public createdDate: Date;

	/**
	 * Date and time when the message was most recently modified (Date).
	 */
	public lastModifiedDate: Date;

	/**
	 * Text displayed in the body of the message (string).
	 */
	public text: string;

	/**
	 * URL of the icon displayed in the body of the message (string).
	 */
	public iconUrl: string;

	/**
	 * Link to a page associated to this message (string).
	 */
	public link: string;

	/**
	 * Array of tags used to filter this message (string[]).
	 */
	public tags: string[];

	/**
	 * Date and time after which the message is no longer displayed nor return when querying the API (Date|null).
	 */
	public expirationDate: Date|null;

	// **************************************************
	// ***                 CONSTRUCTOR                ***
	// **************************************************

	/**
	 * Initialiezs a new instance of the Message model.
	 * 
	 * @param id (string) Unique identifier of the message.
	 * @param createdDate (Date): Date and time when the message was first created.
	 * @param lastModifiedDate (Date): Date and time when the message was most recently modified.
	 * @param text (string): Text displayed in the body of the message.
	 * @param iconUrl (string): URL of the icon displayed in the body of the message.
	 * @param link (string): Link to a page associated to this message.
	 * @param tags (string[]): Array of tags used to filter this message.
	 * @param expirationDate (Date): Date and time after which the message is no longer displayed nor return when querying the API.
	 */
	public constructor (
		id: string = '',
		createdDate: Date = new Date(),
		lastModifiedDate: Date = new Date(),
		text: string = '',
		iconUrl: string = '',
		link: string = '',
		tags: string[] = [],
		expirationDate: Date |null = null
		)
	{
		this.id = id;
		this.createdDate = createdDate;
		this.lastModifiedDate = lastModifiedDate;
		this.text = text;
		this.iconUrl = iconUrl;
		this.link = link;
		this.tags = tags;
		this.expirationDate = expirationDate;
	}

} // class Message

/**
 * Alias type for array of messages (Message[]).
 */
export type MessageArray = Message[];
