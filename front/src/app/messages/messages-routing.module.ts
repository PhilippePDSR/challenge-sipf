// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Pages
import { MessageCreatePage } from './pages/message-create-page/message-create-page.page';
import { MessageEditPage } from './pages/message-edit-page/message-edit-page.page';
import { MessageDeletePage } from './pages/message-delete-page/message-delete-page.page';
import { MessageDetailsPage } from './pages/message-details-page/message-details-page.page';
import { MessagesListPage } from './pages/messages-list-page/messages-list-page.page';

// Guards
import { AuthentificationGuard } from '../authentification/guards/authentification-guard.guard';

// ##################################################
// ###                   ROUTES                   ###
// ##################################################

/**
 * Routes used to interact with messages.
 * They are all prefixed with '/messages'.
 */
const routes : Routes = [
	{ path: 'create', component: MessageCreatePage, canActivate: [ AuthentificationGuard ] },
	{ path: 'edit/:id', component: MessageEditPage, canActivate: [ AuthentificationGuard ] },
	{ path: 'delete/:id', component: MessageDeletePage, canActivate: [ AuthentificationGuard ] },
	{ path: 'details/:id', component: MessageDetailsPage, canActivate: [ AuthentificationGuard ] },
	{ path: '', component: MessagesListPage, canActivate: [ AuthentificationGuard ] }
	];

// ##################################################
// ###        CLASS MESSAGES-ROUTING-MODULE       ###
// ##################################################

/**
 * Module containing the routing of the messages module.
 */
@NgModule( {
	imports: [ RouterModule.forChild( routes ) ],
	exports: [ RouterModule ]
} )
export class MessagesRoutingModule
{}
