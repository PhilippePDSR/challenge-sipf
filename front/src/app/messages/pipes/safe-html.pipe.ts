
import { Pipe } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'

@Pipe( { name: 'safeHtml' } )
export class SafeHtmlPipe
{
    private sanitizer: DomSanitizer

    constructor ( sanitizer: DomSanitizer )
    {
        this.sanitizer = sanitizer;
    }

    transform ( value: any, args?: any ): any
    {
        return this.sanitizer.bypassSecurityTrustHtml( value );
       //return this.sanitizer.bypassSecurityTrustStyle( value );
        // return this.sanitizer.bypassSecurityTrustXxx( value ); - see docs
    }
}
