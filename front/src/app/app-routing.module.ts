// ##################################################
// ###                DEPENDANCES                 ###
// ##################################################

// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Pages
import { HomePage } from './home-page.page';

// ##################################################
// ###                   ROUTES                   ###
// ##################################################

const routes: Routes = [
	{ path: 'home', component: HomePage },
	{ path: 'messages', loadChildren: () => import( './messages/messages.module' ).then( module => module.MessagesModule ) },
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	//{ path: '**', redirectTo: '/home' }
	];

// ##################################################
// ###          CLASS APP-ROUTING-MODULE          ###
// ##################################################

/**
 * Module containing the routing of the application.
 */
@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule
{}
