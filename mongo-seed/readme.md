# MONGO-SEED

Docker container used to seed the MongoDB database using a json file

## Export the DB
```cmd
mongoexport\
	--uri="mongodb://localhost:27017/sipf-message"\
	--collection messages\
	--jsonArray
	--out messages.json
```

## Import the DB
```cmd
mongoimport\
	--uri="mongodb://localhost:27017/sipf-message"\
	--collection messages\
	--jsonArray
	messages.json
```
