# Run with docker compose
```cmd
docker-compose up -d --build
```

# Run Manually

## 1 - Run the MongoDB server
```cmd
mongod --dbpath PATH_TO_DB_FOLDER
```
See also 'mongo-seed/readme.md' for loading json exports.

## 2 - Package and Run the Spring Boot API
```cmd
cd api
set JAVA_HOME=C:\Program Files\Java\jdk-18.0.2.1
.\mvnv.cmd package
java -jar .\target\api-messages-1.0.jar
```

## 3 - Generate the Angular fronten
```cmd
cd front
ng serve
```